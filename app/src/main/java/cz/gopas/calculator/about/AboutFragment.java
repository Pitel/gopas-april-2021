package cz.gopas.calculator.about;

import androidx.fragment.app.Fragment;

import cz.gopas.calculator.R;

public class AboutFragment extends Fragment {
    public AboutFragment() {
        super(R.layout.fragment_about);
    }
}

//Kotlin: class AboutFragment: Fragment(R.layout.fragment_about)