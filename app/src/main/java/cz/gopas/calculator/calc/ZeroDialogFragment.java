package cz.gopas.calculator.calc;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import cz.gopas.calculator.R;

public class ZeroDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //setCancelable(false);
        return new MaterialAlertDialogBuilder(requireContext())
                .setMessage(R.string.zerodiv)
                .create();
    }
}
