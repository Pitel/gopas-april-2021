package cz.gopas.calculator.history;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;


public class HistoryAdapter extends ListAdapter<HistoryEntity, HistoryAdapter.HistoryViewHolder> {

    private static final String TAG = HistoryAdapter.class.getSimpleName();

    private final Consumer<Float> onClick;

    protected HistoryAdapter(Consumer<Float> onClick) {
        super(DIFF);
        setHasStableIds(true);
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder");
        return new HistoryViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(android.R.layout.simple_list_item_1, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder" + position);
        HistoryEntity item = getItem(position);
        ((TextView) holder.itemView).setText(
                String.valueOf(
                        item.value
                )
        );
        holder.itemView.setOnClickListener(v -> onClick.accept(item.value));
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).id;
    }

    class HistoryViewHolder extends RecyclerView.ViewHolder {
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    static DiffUtil.ItemCallback<HistoryEntity> DIFF = new DiffUtil.ItemCallback<HistoryEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull HistoryEntity oldItem, @NonNull HistoryEntity newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull HistoryEntity oldItem, @NonNull HistoryEntity newItem) {
            return oldItem.id == newItem.id && oldItem.value == newItem.value;
        }
    };
}
