package cz.gopas.calculator.history;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import cz.gopas.calculator.databinding.FragmentHistoryBinding;

public class HistoryFragment extends Fragment {
    private static String TAG = HistoryFragment.class.getSimpleName();
    public static String HISTORY_KEY = "history";

    @Nullable
    private FragmentHistoryBinding binding = null;

    private HistoryViewModel viewModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        binding = FragmentHistoryBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.recycler.setHasFixedSize(true);
        final HistoryAdapter adapter = new HistoryAdapter(v -> {
            Log.d(TAG, String.valueOf(v));
            final NavController navController = NavHostFragment.findNavController(this);
            navController.getPreviousBackStackEntry().getSavedStateHandle().set(HISTORY_KEY, v);
            navController.popBackStack();
        });
        binding.recycler.setAdapter(adapter);

        viewModel.history.observe(getViewLifecycleOwner(), historyEntities -> {
            Log.d(TAG,String.valueOf(historyEntities.size()));
            adapter.submitList(historyEntities);
        });
    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }
}
