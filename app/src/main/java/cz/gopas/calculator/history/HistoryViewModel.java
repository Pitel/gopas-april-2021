package cz.gopas.calculator.history;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.util.List;

public class HistoryViewModel extends AndroidViewModel {
    private HistoryDatabase db = null;
    public LiveData<List<HistoryEntity>> history = null;

    public HistoryViewModel(@NonNull Application application) {
        super(application);
        db = Room.databaseBuilder(getApplication(), HistoryDatabase.class, "history")
                .allowMainThreadQueries() // DON'T DO THAT IN PRODUCTION!
                .build();
        history = db.historyDao().getAll();
    }

    public void insert(HistoryEntity... items) {
        db.historyDao().insert(items);
    }
}
